<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogNews extends Migration
{
    public function up()
    {
        Schema::rename('rw_blog_noticias', 'rw_blog_news');
    }
    
    public function down()
    {
        Schema::rename('rw_blog_news', 'rw_blog_noticias');
    }
}
