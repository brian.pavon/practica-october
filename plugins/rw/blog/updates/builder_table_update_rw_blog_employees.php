<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogEmployees extends Migration
{
    public function up()
    {
        Schema::rename('rw_blog_employers', 'rw_blog_employees');
    }
    
    public function down()
    {
        Schema::rename('rw_blog_employees', 'rw_blog_employers');
    }
}
