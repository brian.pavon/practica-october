<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogCategories3 extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_categories', function($table)
        {
            $table->integer('sort_order');
        });
    }
    
    public function down()
    {
        Schema::table('rw_blog_categories', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
