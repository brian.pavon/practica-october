<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogNews2 extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_news', function($table)
        {
            $table->timestamp('published_at');
            $table->renameColumn('status', 'is_published');
            $table->dropColumn('publication_date');
            $table->dropColumn('sort_order');
        });
    }
    
    public function down()
    {
        Schema::table('rw_blog_news', function($table)
        {
            $table->dropColumn('published_at');
            $table->renameColumn('is_published', 'status');
            $table->date('publication_date');
            $table->integer('sort_order')->default(0);
        });
    }
}
