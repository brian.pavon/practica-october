<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRwBlogNoticias extends Migration
{
    public function up()
    {
        Schema::create('rw_blog_noticias', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 50);
            $table->string('slug', 100);
            $table->text('description');
            $table->dateTime('publication_date');
            $table->integer('id_category');
            $table->integer('status')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('rw_blog_noticias');
    }
}
