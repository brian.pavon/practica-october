<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogTags2 extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_tags', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
    
    public function down()
    {
        Schema::table('rw_blog_tags', function($table)
        {
            $table->integer('sort_order')->default(1);
        });
    }
}
