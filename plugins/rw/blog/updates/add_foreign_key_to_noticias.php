<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddForeignKeyToNoticias extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->foreign("id_category")->references('id')->on('rw_blog_categories')
            ->onUpdate('cascade')
            ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->dropForeign(["rw_blog_noticias_id_category_foreign"]);
        });
    }
}
