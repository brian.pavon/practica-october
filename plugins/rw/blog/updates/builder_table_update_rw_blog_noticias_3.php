<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogNoticias3 extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->integer('id_category')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->integer('id_category')->unsigned(false)->change();
        });
    }
}
