<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogCategories4 extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_categories', function($table)
        {
            $table->integer('sort_order')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('rw_blog_categories', function($table)
        {
            $table->integer('sort_order')->default(null)->change();
        });
    }
}
