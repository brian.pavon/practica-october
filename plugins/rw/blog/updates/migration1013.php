<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1013 extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->index('slug');
        });
    }

    public function down()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->dropIndex('rw_blog_noticias_slug_index');
        });
    }
}
