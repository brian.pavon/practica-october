<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogNoticias extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->date('publication_date')->nullable(false)->unsigned(false)->default(null)->change();
            $table->integer('id_category')->nullable()->change();
            $table->boolean('status')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->dateTime('publication_date')->nullable(false)->unsigned(false)->default(null)->change();
            $table->integer('id_category')->nullable(false)->change();
            $table->integer('status')->nullable(false)->unsigned(false)->default(0)->change();
        });
    }
}
