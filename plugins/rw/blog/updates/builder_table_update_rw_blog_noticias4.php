<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogNoticias4 extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->renameColumn('id_category', 'category_id');
        });
    }
    
    public function down()
    {
        Schema::table('rw_blog_noticias', function($table)
        {
            $table->renameColumn('category_id', 'id_category');
        });
    }
}