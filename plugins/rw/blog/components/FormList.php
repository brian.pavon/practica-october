<?php

namespace Rw\Blog\Components;

use System\Models\File;
use Rw\Blog\Models\Employee;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use October\Rain\Support\Facades\Flash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class FormList extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'FormList Component',
            'description' => 'Formulario de contacto'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSubmitForm()
    {

        $data = post();

        $rules = [
            'name' => 'required|string|min:4',
            'surname' => 'required|string|min:4',
            'email' => 'required|email',
            'telephone' => 'required|numeric'
        ];

        $validation = \Validator::make($data, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $employee = new Employee();
        $employee->name = post('name');
        $employee->surname = post('surname');
        $employee->email = post('email');
        $employee->telephone = post('telephone');
        $employee->file = \Input::file('file');

        $employee->save();
        try {
            Mail::send('rw.blog::mail.message', $data, function ($message) use ($data) {
                $message->from('practica-october@october.com');
                $message->to($data['email']);
                $message->subject('Cv Recibido.');
            });
        } catch (\Throwable $th) {
            \Flash::error($th->getMessage());
            \Flash::error('No se pudo enviar el CV');
        }
        \Flash::success('Cv enviado Correctamente!');
    }
}
