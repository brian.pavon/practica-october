<?php
namespace Rw\Blog\Components;

use Cms\Classes\ComponentBase;
use Rw\Blog\Models\Post;
use Rw\Blog\Models\Category;

class PostList extends ComponentBase
{


    public function componentDetails()
    {
        return [
            'name'        => 'Listado de Noticias',
            'description' => 'Se mostraran todas las noticias'
        ];
    }


    public function onRun()
    {
        $post = new Post();
        $this->page['columns'] = $this->property('columns');
        $category = $this->property('category');
        if ( $category != '') {
            $this->page['noticias'] = $post->where('category_id', $category)->get();
        }
        $this->page['news'] = $post->get();
    }

    public function defineProperties()
    {
        return [
            'columns' => [
                'title'             => 'Columnas',
                'description'       => 'Tamaño máximo a visualizar',
                'default'           => 3,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Solo acepta números.'
            ],
            'category' => [
                'title'       => 'Category',
                'type'        => 'dropdown',
                'default'     => ''
            ]
        ];
    }
    public function getCategoryOptions()
    {
        $category = new Category();
        $result = [];
        $result[''] = 'Sin Filtro';
        $data = $category->get();
        foreach ($data as $item) {
            $result[$item->id] = $item->title;
        }
        return $result;
    }
}
