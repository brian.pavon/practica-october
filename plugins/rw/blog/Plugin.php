<?php
namespace Rw\Blog;

use System\Classes\PluginBase;
use Rw\Blog\Components\PostList;
use Rw\Blog\Components\FormList;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            PostList::class => 'PostList',
            FormList::class => 'FormList'
        ];
    }

    public function registerSettings()
    {
    }

    public function registerMailTemplates()
    {
        return [
            'rw.blog::mail.message',
        ];
    }
}
