<?php

namespace Rw\Blog\Models;

use Model;
use System\Models\File;

/**
 * Model
 */
class Employee extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'rw_blog_employees';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $attachOne = [
        'file' => File::class
    ];
}
