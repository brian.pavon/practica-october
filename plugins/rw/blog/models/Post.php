<?php
namespace Rw\Blog\Models;

use Model;
use System\Models\File;

/**
 * Model
 */
class Post extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement =[ 'RainLab.Translate.Behaviors.TranslatableModel' ];

    public $translatable = ['title',['slug', 'index' => true],'description'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'rw_blog_news';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $attachOne = [
        'image' => File::class
    ];
    public $belongsTo = [
        'category' => Category::class
    ];
}
