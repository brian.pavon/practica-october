<?php namespace Rw\Blog\Models;

use Model;

/**
 * Model
 */
class Tag extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title'];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'rw_blog_tags';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
