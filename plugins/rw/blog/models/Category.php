<?php

namespace Rw\Blog\Models;

use Model;
use Rw\Blog\Models\Noticia;
use Rw\Blog\Models\Post;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title'];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'rw_blog_categories';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'news' => Post::class,
        'news_count' => [Post::class, 'count' => true]
    ];
}
