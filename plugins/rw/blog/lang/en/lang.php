<?php
return [
    'plugin' => [
        'name' => 'blog',
        'description' => '',
    ],
    'fields' => [
        'title' => 'Title',
        'slug' => 'Slug',
        'description' => 'Description',
        'status' => 'Status',
        'publication_date' => 'Publication Date',
        'image' => 'Image',
        'category' => 'Category',
        'name' => 'Name',
        'surname' => 'Surname',
        'email' => 'Email',
        'telephone' => 'Telephone',
        'file' => 'File',
        'upload' => 'Upload',
        'is_published' => 'Is Published',
        'published_at' => 'Published At',
    ],
    'menu' => [
        'name' => 'Blog',
        'noticias' => 'Noticias',
        'categories' => 'Categories',
        'tags' => 'Tags',
        'employees' => 'Employees',
        'news' => 'News',
    ],
    'field' => [
        'title' => 'Title',
    ],
    'fields-title' => 'Title',
];
